<?php
require_once("../../../vendor/autoload.php");

use App\profilePicture;
use App\Utility\Utility;

$obj = new profilePicture\ProfilePicture();

$obj->setData($_GET);

$obj->recover();

Utility::redirect("index.php");