<?php
require_once ("../../../vendor/autoload.php");

$objProfilePic = new \App\profilePicture\ProfilePicture();

$fileName = time().$_FILES['image']['name'];

$source = $_FILES['image']['tmp_name'];
$destination = "images/".$fileName;
move_uploaded_file($source,$destination);

$_POST['profilePic'] =  $fileName;

$objProfilePic->setData($_POST);
$objProfilePic->store();

\App\Utility\Utility::redirect("index.php");