<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SpartaN</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <!--font awesome-->
    <link rel="stylesheet" href="../../resources/bootstrap/css/font-awesome.min.css">
    <!--animations css-->
    <link rel="stylesheet" href="../../resources/bootstrap/css/animations.css">
    <!-- custom style-->
    <link rel="stylesheet" href="../../resources/bootstrap/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header id="header">
    <nav class="navbar navbar-fixed-top navbar-default"  id="navbar-container">
        <div class="container-fluid">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-default navbar-toggle collapsed" data-toggle="collapse" data-target="#topMenu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar" style="color: #ebccd1></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar right" id="topMenu">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="home.php" onclick="$('#carousel-section').animatescroll();">TEAM SPARTAN</a></li>
                        <li><a href="Book_Title/index.php" onclick="$('#service-section').animatescroll({padding:110});">BOOK TITLE</a></li>
                        <li><a href="profilePicture/index.php" onclick="$('#portfolio-section').animatescroll({padding:165});">PROFILE PICTURE</a></li>
                        <li><a href="Gender/index.php" onclick="$('#portfolio-section').animatescroll({padding:165});">GENDER</a></li>
                        <li><a href="Hobbies/index.php" onclick="$('#portfolio-section').animatescroll({padding:165});">HOBBIES</a></li>
                        <li><a href="City/index.php" onclick="$('#portfolio-section').animatescroll({padding:165});">CITY</a></li>
                        <li><a href="SummeryOfOrg/index.php" onclick="$('#portfolio-section').animatescroll({padding:165});">SUMMERRY OF ORG </a></li>
                        <li><a href="Birthday/index.php" onclick="$('#portfolio-section').animatescroll({padding:165});">BIRTHDAY</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </div><!-- /.container-fluid -->
    </nav>
</header><!-- header -->

<section id="carousel-section">
    <div id="img-text" class="animatedParent">
        <h2><span class="animated bounceInLeft">Atomic Project</span> <br>
            <span class="animated bounceInRight">CREATED BY</span> <br>
            <span class="animated bounceInRight">TEAM SPARTAN - B59</span> </h2>
    </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script src="../../resources/bootstrap/js/jquery1.12.4.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../../resources/bootstrap/js/parallax.min.js"></script>
<script src="../../resources/bootstrap/js/animatescroll.min.js"></script>
<script src="../../resources/bootstrap/js/css3-animate-it.js"></script>
<!--custom script-->
<!--<script src="js/custom.js"></script>-->
</body>
</html>