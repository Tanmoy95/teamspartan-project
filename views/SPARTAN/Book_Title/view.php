<?php
require_once ("../../../vendor/autoload.php");


$obj = new \App\BookTitle\BookTitle();
$obj->setData($_GET);
$singleData = $obj->view();
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title View</title>

    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">

</head>
<body class="container">

    <h1> Single Record Information - Book Title</h1>
    <a href="index.php" class="btn btn-primary">Back</a>
   <table class="table table-bordered table-striped">

       <tr>

           <th>ID</th>
           <th>Book Title</th>
           <th>Author Name</th>

       </tr>

   <?php


     echo "
       <tr>
            <td>$singleData->id</td>
            <td>$singleData->book_title</td>
            <td>$singleData->author_name</td>

       </tr>

     ";


       ?>



   </table>



</body>
</html>
