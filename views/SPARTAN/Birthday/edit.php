<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Utility\Utility;

use App\Message\Message;

if(!isset($_GET['id'])) {
    Message::message("You can't visit view.php without id (ex: view.php?id=23)");
    Utility::redirect("index.php");
}


$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";


$obj = new \App\Birthday\Birthday();

$obj->setData($_GET);

$singleData  =  $obj->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Add Form</title>
<!--    <link rel="stylesheet" href="../../../resources/bootstrap/css/formstyle.css">-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">

    <style>
        body {
            background:antiquewhite;
        }
    </style>
</head>
<body  class="container">
<div class="container">
    <h1 style="color: #442a8d;">Update Birthday</h1>

    <form class="form-horizontal" action="update.php" method="post">

        <strong> Please Enter Name:</strong>
        <input type="text" name="name" value="<?php echo $singleData->name ?>">
        <br>

        <strong>Please Enter Author Name: </strong>
        <input type="text" id="datepicker" name="date" value="<?php echo $singleData->birth_day?>">
        <br>

        <!-- ////////////////////////////////////////////////////////  -->
        <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
        <!-- ////////////////////////////////////////////////////////  -->

        <input type="submit" value = "Update">

    </form>
</div>


<script src="../../../resources/bootstrap/js/jquery.js"></script>
<script src="../../../resources/bootstrap/js/jquery1.12.4.js"></script>
<script src="../../../resources/bootstrap/js/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker();
    } );
</script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>