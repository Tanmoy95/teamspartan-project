<?php
require_once ("../../../vendor/autoload.php");


$obj = new \App\Hobies\Hobies();
$obj->setData($_GET);
$singleData = $obj->view();
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies</title>

    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">

</head>
<body class="container">

<h1> Single Record Information - Hobby</h1>
<a href="index.php" class="btn btn-primary">Back</a>

<table class="table table-bordered table-striped">

    <tr>

        <th>ID</th>
        <th>Name</th>
        <th>Hobby</th>

    </tr>

    <?php


    echo "
       <tr>
            <td>$singleData->id</td>
            <td>$singleData->name</td>
            <td>$singleData->hobies</td>

       </tr>

     ";


    ?>



</table>



</body>
</html>
