<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/hobies.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Hobby Insert</title>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
</head>
<body class="container">
<div class="container width" style="padding-top: 2%">
    <form action="store.php" method="post">
        <div class="panel panel-default form-horizontal" >
            <div class="panel-heading">
                <h3 class="text-center">Add Your Hobbies</h3
            </div>
            <div class="panel-body"">
            <table class="table table-striped">
                <tr>
                    <td>Enter Name:</td>
                    <td>
                        <input type="text" class="form-control" name="name" placeholder="Enter Student Name Here..." required>
                    </td>
                </tr>
                <tr>
                    <td>Hobby:</td>
                    <td>
                        <div class="checkbox">
                            <input type="checkbox" name="hobie"  value="Gardening" > Gardening<br/>
                            <input type="checkbox" name="hobie"  value="Book Reading" > Book Reading<br/>
                            <input type="checkbox" name="hobie"  value="Photography" > Photography<br/>
                            <input type="checkbox" name="hobie"  value="Travelling" > Travelling<br/>
                            <input type="checkbox" name="hobie"  value="Drawing" > Drawing<br/>
                            <input type="checkbox" name="hobie"  value="Fishing" > Fishing<br/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <button type="submit" name="submit" class="btn btn-success">Add</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>
</body>
</html>