<?php
require_once("../../../vendor/autoload.php");

use App\Hobies\Hobies;
use App\Utility\Utility;

$obj = new Hobies();

$obj->setData($_GET);

$obj->trash();

Utility::redirect("trashed.php");