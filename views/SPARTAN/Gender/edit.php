<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Utility\Utility;

use App\Message\Message;

if(!isset($_GET['id'])) {
    Message::message("You can't visit view.php without id (ex: view.php?id=23)");
    Utility::redirect("index.php");
}


$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";


$obj = new \App\Gender\Gender();

$obj->setData($_GET);

$singleData  =  $obj->view();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/formstyle.css">
    <style>
        body {
            background: #67b168;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <h2>Gender Edit Form</h2>
        <form class="form-horizontal" action="update.php" method="post">

            <!--///////////////////////////////////////////////////////-->
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $singleData->id ?>">
            <!--///////////////////////////////////////////////////////-->

            <div class="form-group">
                <label class="control-label col-sm-3" for="name">Name:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $singleData->user_name ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3" for="hobbies">Select Gender: </label>
                <div class="col-sm-9">
                    <div style="height: 7px"></div>
                    <input type="radio" name="Gender" value = "Male" <?php if($singleData->gender == "Male") echo 'checked="checked"' ?>>Male
                    <input type="radio" name="Gender" value = "Female" <?php if($singleData->gender == "Female") echo 'checked="checked"' ?>>Female



                </div>
            </div>

            <div class="form-group">
                <input type="submit" value="Update">
            </div>
        </form>
    </div>
    <div class="col-lg-3"></div>
</div>
<script src="../../../resources/bootstrap/js/jquery.js"></script>


    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
</script>


</body>
</html>