<?php
require_once ("../../../vendor/autoload.php");

$objSummery = new \App\SummeryOfOrg\Summery();
$objSummery->setData($_POST);
$objSummery->store();

\App\Utility\Utility::redirect("index.php");