<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Utility\Utility;

use App\Message\Message;

if(!isset($_GET['id'])) {
    Message::message("You can't visit view.php without id (ex: view.php?id=23)");
    Utility::redirect("index.php");
}


$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";


$obj = new \App\City\City();

$obj->setData($_GET);

$singleData  =  $obj->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Add Form</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/formstyle.css">
    <style>
        body {
            background: #c1e2b3;
        }
    </style>
</head>
<body>
<div class="container">

    <h1 style="color: #442a8d;">City Information Edit </h1>

    <form action="update.php" method="post">

        <strong> Please Enter Name:</strong>
        <input type="text" name="name" value="<?php echo $singleData->name ?>">
        <br>

        <strong>Please Enter City: </strong>
        <div class="form-group">

            <div class="country">
                <select name="country" required>
<!--                    <option value="" disabled selected>Please Select city...</option>-->
                    <option name="country" value="Dhaka">Dhaka</option>
                    <option name="country" value="Chittagong">Chittagong</option>
                    <option name="country" value="Barishal">Barishal</option>
                    <option name="country" value="Khulna">Khulna</option>
                    <option name="country" value="RajSahi">RajSahi</option>
                    <option name="country" value="RangPur">RangPur</option>
                    <option name="country" value="Syllet">Syllet</option>
                    <option name="country" value="Comilla">Comilla</option>
                    <option name="country" value="Narayenghonj">Narayenghonj</option>
                    <option name="country" value="Cox's Bazer">Cox's Bazer</option>

                </select>
            </div>

        </div>
        <br>

        <!-- ////////////////////////////////////////////////////////  -->
                    <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
        <!-- ////////////////////////////////////////////////////////  -->

                     <input type="submit" value = "Update">

    </form>


</div>
<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>