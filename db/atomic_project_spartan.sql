-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2017 at 08:52 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_spartan`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE `birth_day` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birth_day` date NOT NULL,
  `is_trashed` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `name`, `birth_day`, `is_trashed`) VALUES
(1, 'Omar Sharif Ansary', '2017-06-14', 'No'),
(2, 'dhasuihduias', '2017-06-01', 'No'),
(3, 'wuhfuheef', '2017-06-07', 'No'),
(4, 'a wrtawhr', '2017-06-02', 'No'),
(5, 'awr5c jcw', '2017-06-21', 'No'),
(6, 'ery', '2017-06-08', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `is_trashed` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_trashed`) VALUES
(4, 't2t4r4', 'Ahmed', 'No'),
(6, 'Mom', 'Hilali', 'No'),
(10, 'Paradoxical Sajid', 'Arif Azad', 'No'),
(11, 'Toy', 'adwerwer', 'No'),
(12, 'rtegwerg', 'qqtt43t', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `city` varchar(20) NOT NULL,
  `is_trashed` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_trashed`) VALUES
(1, 'Shamimrwerwer', 'Dhaka', '2017-06-18 02:20:04'),
(2, 'Nila', 'Syllet', 'No'),
(3, 'Munna', 'Australia', '2017-06-18 02:20:34'),
(7, 'Munnarqr', 'Khulna', 'No'),
(8, 'x gdg', 'Comilla', 'No'),
(9, 'jerty', 'Khulna', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `is_trashed` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `user_name`, `gender`, `is_trashed`) VALUES
(1, 'Zidan', 'Male', 'No'),
(2, 'Shamim', 'male', 'No'),
(3, 'Tahsina', 'Female', 'No'),
(4, 'Touhida', 'Female', 'No'),
(5, 'kafu', 'Male', 'No'),
(6, 'omar', 'Male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobies`
--

CREATE TABLE `hobies` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hobies` varchar(100) NOT NULL,
  `is_trashed` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobies`
--

INSERT INTO `hobies` (`id`, `name`, `hobies`, `is_trashed`) VALUES
(1, 'Shmaim23', 'Cooking, Coloring, Magic', 'No'),
(2, 'Nila', 'Cooking, Coloring, Drawing, Fashion', 'No'),
(3, 'kafu', 'Cooking, Drawing, Fashion, Magic', 'No'),
(4, 'Econ', 'Coloring, Drawing, Fashion', 'No'),
(5, 'Nusi', 'Cooking,Coloring,Fashion', 'No'),
(6, 'Rista', 'Cooking,Coloring', 'No'),
(7, 'sbdhbasd', 'Cooking, Coloring, Drawing', 'No'),
(8, '64e6546', 'Cooking, Coloring, Fashion', 'No'),
(9, 'imran', 'Array', 'No'),
(10, 'wefrewrr', 'Array', 'No'),
(11, 'ftfhtf', 'Array', 'No'),
(12, 'hgdh', 'Array', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `profile_picture` varchar(100) NOT NULL,
  `is_trashed` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `is_trashed`) VALUES
(1, 'Shmaim', '1497767979Capture1.PNG', 'No'),
(2, 'Shamim12', '1496058486html-color-codes-color-tutorials-903ea3cb.jpg', 'No'),
(3, 'Nila', '1497767998EventImage.PNG', 'No'),
(4, 'Mishu', '1496059327greenlight.png', 'No'),
(5, 'Remon', '1497768136Capture.PNG', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summery`
--

CREATE TABLE `summery` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `summery` varchar(255) NOT NULL,
  `is_trashed` varchar(100) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summery`
--

INSERT INTO `summery` (`id`, `name`, `summery`, `is_trashed`) VALUES
(1, 'DANO', 'World class Full cream milk we provide.', 'No'),
(2, 'Shamim', 'jjj', 'No'),
(5, 'Sharif', 'ki obostrha adff f', 'No'),
(7, 'Karim', 'jasduihasfhnao;dfj iajsdfipjas fasif w', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobies`
--
ALTER TABLE `hobies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summery`
--
ALTER TABLE `summery`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobies`
--
ALTER TABLE `hobies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `summery`
--
ALTER TABLE `summery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
