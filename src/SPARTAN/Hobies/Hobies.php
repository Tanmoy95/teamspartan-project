<?php
namespace App\Hobies;
use PDO;
use App\Utility\Utility;

use App\Message\Message;
use App\Model\Database;
class Hobies extends Database
{
    public $id;
    public $name;
    public $hobie;

    public function setData($rcv){
        if (array_key_exists('id',$rcv)){

            $this->id = $rcv['id'];
        }

        if (array_key_exists('name',$rcv)){

            $this->name = $rcv['name'];
        }
        if (array_key_exists('hobie',$rcv)){


            $this->hobie = $rcv['hobie'];

        }
    }

    public function store(){

        $name = $this->name;
        $hobie = $this->hobie;
        $dataArray = array($name,$hobie);

        $sql = "INSERT INTO `hobies` (`name`, `hobies`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully!! Data Inserted....");
        }else{

            Message::message("Error!!Please right this error...");
        }

    }
    public function index(){

        $sqlQuery = "Select * from hobies where is_trashed='No'";

        $STH = $this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }

    /**
     * @return mixed
     */
    public function view(){

        $query = "SELECT * FROM hobies WHERE id=".$this->id;

        $STH = $this->db->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();
        return $singleData;

    }
    public function update(){

        $query = "UPDATE hobies SET name = ?,hobies = ? WHERE id = $this->id;";

        //Utility::dd($query);

        $dataArray = array($this->name, $this->hobie);

        $STH = $this->db->prepare($query);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Success :) Data Updated Successfully.");
        }
        else{
            Message::message("Failure :( Data Not Updated!");
        }
    }
    public function trash(){

        $sqlQuery = "UPDATE hobies SET is_trashed=NOW() WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }
    public function trashed(){

        $sqlQuery = "Select * from hobies where is_trashed <> 'No'";

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function recover(){

        $sqlQuery = "UPDATE hobies SET is_trashed='No' WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }





    public function delete(){

        $sqlQuery = "DELETE from hobies  WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Delete operation is not possible due to an error.");

        }

    }
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byHobby']) )  $sql = "SELECT * FROM `hobies` WHERE `is_trashed` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `hobies` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byHobby']) ) $sql = "SELECT * FROM `hobies` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byHobby']) )  $sql = "SELECT * FROM `hobies` WHERE `is_trashed` ='No' AND `hobies` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->hobies);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->hobies);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from hobies  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }












}