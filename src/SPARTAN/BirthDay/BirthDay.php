<?php

namespace App\Birthday;
use App\Utility\Utility;
use PDO;
use App\Model\Database;
use App\Message\Message;

class Birthday extends Database
{
    public $id;
    public $name;
    public $birthday;
    public function setData($rcv){
        if (array_key_exists("id",$rcv)){

            $this->id = $rcv['id'];
        }

        if (array_key_exists("name",$rcv)){

            $this->name = $rcv['name'];
        }
        if (array_key_exists("date",$rcv)){

            if(!empty($rcv['date'])) {
                date_default_timezone_set('Asia/Dhaka');
                $strBirth = strtotime($rcv['date']);
                $this->birthday = date('Y-m-d', $strBirth);
            }else{
                $strBirth ="";
            }
        }
    }
    public function store(){

        $name = $this->name;
        $birth_day = $this->birthday;

        $dataArray = array($name,$birth_day);

        $sqlQuery ="INSERT INTO `birth_day` (`name`, `birth_day`)  VALUES (?,?);";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }


    }
    public function index(){

        $sqlQuery = "Select * from birth_day where is_trashed='No'";

        $STH = $this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }
    public function view(){

        $sqlQuery = "Select * from birth_day where id=".$this->id;

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function update(){

        $sqlQuery = "UPDATE birth_day SET name = ?,birth_day = ? WHERE id = $this->id;";

        //    Utility::dd($sqlQuery);


        $dataArray = array($this->name, $this->birthday);

        $STH = $this->db->prepare($sqlQuery);

        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Success :) Data has been updated successfully.");
        }
        else {
            Message::message("Failure :( Update is not possible due to an error.");

        }

    }// end of store()
    public function trash(){

        $sqlQuery = "UPDATE birth_day SET is_trashed=NOW() WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }
    public function trashed(){

        $sqlQuery = "Select * from birth_day where is_trashed <> 'No'";

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function recover(){

        $sqlQuery = "UPDATE birth_day SET is_trashed='No' WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }
    public function delete(){

        $sqlQuery = "DELETE from birth_day  WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Delete operation is not possible due to an error.");

        }

    }
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byDate']) )  $sql = "SELECT * FROM `birth_day` WHERE `is_trashed` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `birth_day` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byDate']) ) $sql = "SELECT * FROM `birth_day` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byDate']) )  $sql = "SELECT * FROM `birth_day` WHERE `is_trashed` ='No' AND `birth_day` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->birth_day);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->birth_day);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from birth_day WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }




}